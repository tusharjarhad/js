'use strict'

// eslint-disable-next-line no-unused-vars
function demoVariables() { //eslint-line-disable
    let admin = 'John'
    let name = admin
    console.log(name)

    const MAX_TIMEOUT = 200
    console.log(MAX_TIMEOUT)

    // This will be error in strict mode 
    // adminName = 'admin'
    // console.log(adminName)
}
demoVariables()


function demoDataTypes(){

    // 7 basic types in JS
    // Number
    console.log(typeof(2))
    console.log(typeof(2.343))
    console.log(typeof(NaN))
    console.log(typeof(Infinity))
    // Casting value = Number(value); 
    // Number(undefined) = NaN
    // Number(null) = 0 

    // String
    console.log(typeof("hello"))
    console.log(typeof(`Hello`))
    let name = 'John'
    console.log('Hello ${name}')
    console.log(`Hello ${name}`)
    // Casting value = String(value); 

    // Boolean
    console.log(typeof(true))
    // Casting value = Boolean(value); 

    // undefined
    console.log(typeof(undefined))   // for unassigned values 

    // Object
    console.log(typeof(null)) // for unknown values
    // For null returns "object" – that’s an error in the language, it’s not an object in fact.
    let b = {}
    console.log(typeof(b)) 

    // Symbol
    console.log(typeof(Symbol("id")))

    // Function
    function sample(){
        return 9
    }
    console.log(typeof(sample))

}

demoDataTypes()


function getGreetings(login){
    let message;
    message = (login == 'Employee') ? 'Hello'          : 
              (login == 'Manager') ?  'Greetings'      :
              (login == '') ? 'No Login'               :
              ''
    return message
}
getGreetings()

let sayHi = function(name) {
    console.log(`Hello ${name}`)
    console.log('Bye Bye')
}
sayHi()

let sum = (a,b) => a + b
console.log(sum(2,3))