'use strict'

function demoRestAndSpread() {
    
    function func1(arg1) {
        console.log(arg1)
        console.log(arguments)
    }
    func1('122', '222', '233', 23233)

    /**
     * 
     * @param {string} bag 
     * @param  {...any} rest 
     * @returns undefined
     */
    function shoppingCart(bag, ...rest){
        console.log(bag)
        console.log(rest)
    }
    shoppingCart('plastic', 'item1', 'item2', 34, 234)
    let soaps = ['1', '2', 'A']
    shoppingCart(...soaps)
    let a = {
        q:1,
        w:2,
        e:3,
    }
    console.log(a)
    // let b = {c:22, ...a, d:11}
    // console.log(b)
}

demoRestAndSpread()

function demoClosure() {
    let name = 'John'

    function sayHi() {
        console.log('Hi ' + name)
    }

    name = 'Pete'

    sayHi()
}

demoClosure()

function demoCounter() {

    function makeCounter() {
        let count  = 0
        return function () {
            return count ++;
        }
    }

    let counter1 = makeCounter()
    let counter2 = makeCounter()

    console.log('\n\nCounter')
    console.log(counter1())
    console.log(counter1())
    
    console.log(counter2())
    console.log(counter2())


    function sum(a){
        return function(b) {
            return a+b
        }
    }
    console.log('\n\n')
    console.log(sum(1)(2))


    console.log('\n\n')
    function makeArmy() {
        let shooters = [];
      
        let i = 0;
        while (i < 10) {
          let a = i  
          let shooter = function() { // shooter function
            console.log( a ); // should show its number
          };
          shooters.push(shooter);
          i++;
        }
      
        return shooters;
      }
      
      let army = makeArmy();
      
      army[0](); // the shooter number 0 shows 10
      army[5](); // and number 5 also outputs 10...
      // ... all shooters show 10 instead of their 0, 1, 2, 3...


}

demoCounter()


function demoFunctionApply(){
    function sayHello(greet, end) {
        console.log(greet + ' ' + this.name + ' ' + end)
    }
    let user = {name: 'John'}
    sayHello.apply(user, ['Hello', 'Bye']) // For Arraylike
    sayHello.call(user, 'Morning', 'Bye Bye!') // For iterables
}

demoFunctionApply()


function demoTimeout(){
    function sayHello() {
        console.log("Timeout !!!!!")
    }
    
    setTimeout(sayHello, 1)
    console.log("Go Go")
}
demoTimeout()