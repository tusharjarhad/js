'use strict'

function demoClass(){

    class Animal {

        constructor(name) {
            this.name = name
        }
    }

    class Rabbit extends Animal {

        constructor(name) {
            super(name)
            this.created = Date.now()
        }

        static printAnimal(){
            console.log("Hello")
        }
    }

    let rabbit = new Rabbit('white')
    console.log(rabbit)
    Rabbit.printAnimal()
    console.log(rabbit instanceof Animal)
    console.log(rabbit instanceof Rabbit)
    
}

demoClass()
