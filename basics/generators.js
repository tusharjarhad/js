'use strict'

function demoGenerator(){

    function *calculator(input){
        var doubleThat = 2 * ( yield (input/ 2))
        var another = yield (doubleThat)
        return (input * doubleThat * another)
    }

    const calc = calculator(10)
    console.log(calc.next())
    console.log(calc.next(7))
    console.log(calc.next(100))
    console.log(calc.next())

    function * gen1(i) {
        console.log(i)
        let j = 5 * (yield (i * 10))
        console.log(j)
        let k = yield(2 * j / 4)
        console.log(k)
        return (i + j + k)
   }
   
   let g1 = gen1(10)
   console.log(g1.next(20))
   console.log(g1.next(10))
   console.log(g1.next(5))


}

demoGenerator()