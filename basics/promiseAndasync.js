'use strict'

function demoPromise(){

    let promise = new Promise(function(resolve, reject){
        // setTimeout(()=> resolve([1,2,3,4], 2000))
        setTimeout(()=> reject('Div by 0', 1000))
    })

    promise.then( r => console.log('Suceess:  '+ r), e=> console.log('Error:  '+e))
    promise.then(r => console.log("!!!!!!!!  " + r), e=> {return e})
    console.log('Hello')
}
// demoPromise()

function demoPromiseChaining() {
    console.log('Hi')

    let p1 = new Promise(function(resolve) {
        setTimeout(()=> resolve(1), 1000)
    })

    p1.then(function(result){
        console.log('R1:'  +result)
        return result * 2
        // return new Promise
    }).then((r) => {
        console.log('R2:'  +r)
        return r * 10
    }).then((r) => {
        console.log('R3:'  +r)
        return r * 20
    })

    p1.then(function(result){
        console.log('r1:'  +result)
        return result + 2
        // return new Promise
    }).then((r) => {
        console.log('r2:'  +r)
        // throw new Error('HKKK')
        return r + 10
    }).then((r) => {
        console.log('r3:'  +r)
        return r + 20
    }).catch((e) => {
        console.log(e)
    }).then((r) => {
        console.log('r4:' + r)
    })
    console.log('AAA')
    
}

// demoPromiseChaining()

function demoAsync() {
    // async function f() {
    //     return Promise.resolve(1);
    //   }
      
    // f().then(console.log('AAA'));
    // console.log('QQQ')

    async function f1() {
        let promise = new Promise( (resolve, reject) => {
            setTimeout(() => resolve("done"), 2000)
        })
        console.log("ZZZZ")
        promise.then(() => console.log("Finish"))
        console.log('YYYY')
        let result = await promise
        console.log("QQQ")
        console.log(result)

    }
    f1()
    console.log('BBBB')
}

demoAsync()
