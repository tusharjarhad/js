'use strict'

function demoProperty() {
    let a = { name: 'John'}
    console.log(Object.getOwnPropertyDescriptor(a, 'name'))
    Object.defineProperty(a, 'b', {value:22, enumerable: false, configurable: true, writable:true})
    Object.defineProperty(a, 'c', {value:22, enumerable: true, configurable: true, writable:false})
    Object.defineProperty(a, 'd', {value:22, enumerable: true, configurable: false, writable:false})
    console.log(a) // enumerable not get printed
    try{    
        a.c = '23'
    }catch(e){
            console.log("Error" + e.name)
    }
    try{
        Object.defineProperty(a, 'd', {value:23, writable:false})
    }catch(e) {
        console.log(e.name)
    }
}

demoProperty()

function demoGetSet() {
    let user = {
        name: 'John',
        surname: 'Smith',

        get fullName () {
            return `${this.name} ${this.surname}`
        },

        set fullName(value) {
            [this.name, this.surname] = value.split(" ");
        }
    }

    console.log(user)
    console.log(user.fullName)
    user.fullName = "Alice Cooper";
    console.log(user)

    console.log(Object.getOwnPropertyDescriptor(user, 'fullName'))
}

demoGetSet()


function demoPrototype() {
    let animal = {
        eats: true,
        walk () {
            if( !this.isSleeping) {
                console.log('I walk')
            }
        },
        sleep () {
            this.isSleeping = true
        }
    
    }
    let rabbit = {
        jumps: true
    }

    function Turtle(name) {
        this.name = name
    }
    Turtle.prototype = animal
    rabbit.__proto__ = animal

    // "animal is the prototype of rabbit" or "rabbit prototypally inherits from animal".
    console.log('\n')
    console.log(rabbit.eats)
    let turtle = new Turtle('t')
    console.log(turtle.eats)
    console.log('\n')
    rabbit.sleep()
    console.log(rabbit.isSleeping)
    console.log(animal.isSleeping)
    console.log(rabbit)
    for(let prop in rabbit){
        console.log(prop)
    }
    console.log(Object.getOwnPropertyDescriptor(rabbit, 'sleep'))
    console.log(Reflect.ownKeys(rabbit))
    
    console.log('\n')
    let fox = Object.create(animal)
    console.log(fox.eats)
    console.log(Object.getPrototypeOf(fox) === animal)
    animal.fly = false
    console.log(fox.fly)
    animal.fly = true
    console.log(fox.fly)

    console.log('\n')
    

}

demoPrototype()

function demoClassUsingPrototype(){

    function User(name, birthday) {
        this._name = name;
        this._birthday = birthday;
      }
    User.prototype.sayHi = function() {
        console.log('Hi ' + this._name)
    }  

    function Admin(email) {
        this._email = email
    } 
    Admin.prototype.sendMail = function() {
        console.log('Sending mail')
    }
    Admin.prototype.__proto__ = User.prototype
    let user = new User('John')
    user.sayHi()

    let admin = new Admin('a@a')
    admin._name = 'Admin'
    admin.sayHi()

    console.log(admin instanceof User)
    
}

demoClassUsingPrototype()
