'use strict'

function demoArrays() {
   
    let fruits = ["Apples", "Pear", "Orange"];
    let shoppingCart = fruits;
    shoppingCart.push("Banana");
    console.log(fruits.length)
    console.log(shoppingCart.pop())
    shoppingCart.unshift('Banana')
    console.log(shoppingCart)
    console.log(shoppingCart.shift())
    for(let f of fruits){
        console.log(f)
    }
    fruits[500] = 'Hello'
    console.log(shoppingCart.length)

    console.log('\n\n')
    let arr  = ['i', 'like', 'js', true]
    console.log(arr)

    arr.splice(1, 1, 'will', 'study')
    console.log(arr)

    console.log(arr.slice(2, 4))
    console.log(arr)
    console.log(Array.isArray(arr))


    let numArr = [1,2,3]
    console.log(numArr.concat([4,5]))
    console.log(numArr)

    console.log(numArr.concat({0:'sdf', 'ww':'ff'}))
    console.log(numArr.concat({0:'sdf', 1:'ff', [Symbol.isConcatSpreadable]:true}))
    console.log(numArr.concat({0:'sdf', 1:'ff', [Symbol.isConcatSpreadable]:true, length:2}))
    console.log(numArr)
    

    let users = [
        {id: 1, name: "John"},
        {id: 2, name: "Pete"},
        {id: 3, name: "Mary"}
      ]
    let user = users.find(item => item.id == 1)
    console.log(user)
    user.name = 'Sherlock'
    console.log(users)
    users.reverse()
    console.log(users)
    console.log(users.join('?'))

    users.forEach(element => {
        console.log(element)
    });


}   
demoArrays()