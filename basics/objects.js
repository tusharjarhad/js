'use strict'

function demoObjects() {
    console.log('\n\n')
    let user = {}
    user.name = 'John'
    user.surname = 'Smith'
    console.log(user)
    user.name = 'Pete'
    delete user.name
    user['eductional degree'] = 'Doctor' 
    user[Symbol('id')] = 2
    console.log(user)

    let user1 = {name : 'Sherlock', surname: 'Holmes'}
    let user2 = {name : 'Sherlock', surname: 'Holmes'}
    console.log(user1 == user2)
    console.log(user1 === user2)
    let user3 = user1
    console.log(user1 == user3)
    console.log(user1 === user3)
    

}
demoObjects()

function demoConstObjects(){
    console.log('\n\n')
    const user = {name: 'John'}
    console.log(user)
    user.name = 'Pete'
    console.log(user)
}
demoConstObjects()


function demoSymbols() {
    console.log('\n\n')
    let symId1 = Symbol('id')
    let symId2 = Symbol('id')
    console.log(symId1 == symId2)
    let symId3 = Symbol.for('id')
    let symId4 = Symbol.for('id')
    console.log( symId4 == symId3)

    let user = { [symId1]: 1, name: 'John'}
    console.log(user)
    console.log(user[symId1])
    console.log(symId1)
    for (let key in user) {
        console.log(key + ':' + user[key])
    }

}
demoSymbols()

function demoObjectMethods() {
    console.log('\n\n')

    let user = {
        name : 'John',
        sayHi() { console.log('Hi' + this.name)},
        bye() {console.log('Bye')},
        greet() {
            let l = () => console.log(this.name)
            l()
        }
    }

    user.sayHi() // . returns (base, name, strict) == (user, sayHi, true) after calling () this gets assign to user
    user['bye']()


    // Error
    // ;(user.name == "John" ? user.hi : user.bye)()
    // let f = user.sayHi
    // f()

}
demoObjectMethods()

function demoPrimitives() {
    console.log('\n\n')
    
    let user = {
        name : 'John',
        age : 23,

        toString() {
            return `{name: "${this.name}"}`
        },
        valueOf() {
            return this.age
        }
    }

    console.log('Hi' + user)
    console.log(user)
    console.log( 10 + user)

}
demoPrimitives()

function demoConstructor() {
    console.log('\n\n')
    function User(name, age) {
        this.name = name
        this.age = age 
        this.sayHi = () => console.log('Hi' + this.name)
        console.log(new.target)
         // return this;  (implicitly)
    }
    let user1 = new User('J', 23)
    // let user2 = User('K', 24)
    console.log(user1)
    // console.log(user2)

}

demoConstructor()