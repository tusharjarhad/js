

describe("A suite", function() {
    it("contains spec with an expectation", function() {
      expect(true).toBe(true)
    })
  })


describe('Check A', function() {
  
  var foo, bar = null

  beforeEach(function() {
    foo = {
      setBar: function(value) {
        bar = value;
      },

      callBar: function(k) {
        return this.setBar(k)
      }
    };

   
    
  });
  
  it("tracks that the spy was called", function() {
    spyOn(foo, 'setBar').and.callThrough()
    foo.callBar(123);
    expect(bar).toEqual(123);
    expect(foo.setBar).toHaveBeenCalled();
  });

  it('track', function() {
    spyOn(foo, 'setBar').and.returnValue(200)
    expect(foo.callBar(2)).toEqual(200)
  })
})

describe('machers', function(){
   
   it('any', function() {
     expect(Infinity).toEqual(jasmine.any(Number))
   })
})




