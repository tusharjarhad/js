'use strict'

const R = require('ramda')
{
    console.log(R.add(2,"3"))
    
}

{
  const logA = (a, b, c) => console.log(` a= ${a} , b= ${b} , c=${c}`)
  const logB = R.curry(logA)
  logB(12)(13)(14)
//   const _ = R.__
  const logC12 = logB(R.__,R.__ , 12)
  logC12(11,19)

  

}

{
    const person = {
        name: 'Randy',
        socialMedia: {
          github: 'randycoulman',
          twitter: '@randycoulman'
        }
      }
    
    const nameLens = R.lens(R.prop('name'), R.assoc('name'))
    // const nameLens = lensProp('name')
    const twitterLens = R.lens(
        R.path(['socialMedia', 'twitter']),
        R.assocPath(['socialMedia', 'twitter'])
    )
    // const twitterLens = lensPath(['socialMedia', 'twitter'])

    console.log(R.view(nameLens, person))
    const newRandy = R.set(twitterLens, '@randy', person)
    console.log(person)
    console.log(newRandy)


    
      

}

