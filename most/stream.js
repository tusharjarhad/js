'use strict'

const M = require('most')

const createStream = function() {
    const h$ = M.of('Hello')
    h$.observe(x => console.log(x))

    const h1$ = M.just('Hello')
    h1$.observe(x => console.log(x))
    
    const promise = new Promise( (resolve) => {
        setTimeout(() => resolve("done"), 2000)
    })

    const promise$ = M.fromPromise(promise)
    promise$.observe((value) => console.log(value))
    console.log("Promise")

    const iter$ = M.from([1,2,3,4])
    iter$.observe( (i) => console.log(i))

    {
        const counter = i => new Promise(resolve => setTimeout(() => resolve(`i=${i}`), 1000))
        // const c$ = M.unfold( i => counter(i).then(content => content), 1)
        // c$.observe( (value) => console.log(value))
        const cGenerator$ = function* (){
            for (let i = 0;; i++){
                yield counter(i)
            }
        }
        const cG$ = M.generate(cGenerator$)
        // cG$.observe(value => console.log(value))  
        // setTimeout(() => cG$.observe(value => console.log(`cG$ = ${value}`) ), 6000) 
    }
}

// createStream()


function handleError() {
    {
        console.log('Using Recover with')
        M.from([1,2,3,4,5])
        .map(function(x) {
            if (x === 3){
                throw 'Error 3'
            }
            return x + 1;
        }).recoverWith( e => {
            console.log(`Error : ${e}`)
            return M.of('Sent')
        })
         .observe( x => console.log(`x = ${x}`));
    }
  
    
    {
        console.log('\n Using forEach')
        M.from([1,2,3,4])
        .map(function(x) {
            if (x === 3){
                throw 'Error 3'
            }
            return x + 1;
        })//.forEach( x => console.log(`x = ${x}`))
        //.then( z => console.log(`end  ${z}`), e=>  console.log(`Error : ${e}`))
    }

    {
        console.log('\n Using Subscribe')
        // M.from([1,2,3,4])
        // .map(function(x) {
        //     if (x === 3){
        //         throw 'Error 3'
        //     }
        //     return x + 1;
        // }).subscribe({
        //     next:  x => console.log(`x = ${x}`),
        //     complete: z => console.log(`end  ${z}`),
        //     error: e=>  console.log(`Error : ${e}`)
        // })
    }
}

// handleError()


function transformingStream() {
    M.from([1,2,3,4])
    .map(x => x * x)
    .observe( x => console.log(x))

    M.from([1,2,3,4])
    .constant(-1)
    .observe( x => console.log(x))

    const plus = (x,y) => x + y
    M.from([1,2,3,4,])
    .scan(plus, 0)
    .observe( x => console.log(x))

    // M.from([1, 2])
	// .chain(x => M.periodic(x * 10).take(5).constant(x))
    // .observe(x => console.log(x));
    
        

}

// transformingStream()

function consume() {
    const add = (x, y) => x + y
    M.from([12,12,32])
    .reduce(add, 0)
    .then(x => console.log(x))
    
}

// consume()

function merging() {
    const add = (x, y) => x + y
    const multiply = (x, y) => x * y
    
    const c$  = M.periodic(1000, 1).scan(add, 0)
    const m$  = M.periodic(3000, 2).scan(multiply, 2)
    const x$ = M.periodic(2000, 'x')
    const y$ = M.periodic(3000, 'y')
    // M.merge(c$, x$).observe(x => console.log(x))

    // M.mergeArray([c$, x$, y$]).observe(x => console.log(x))
    // c$.combine(add, m$).observe(x => console.log(x))    
    //  c$.zip(add, m$).observe(x => console.log(x)) 
    // c$.sampleWith(y$).observe(x => console.log(x))
}
// merging()


function higherOrder() {

    const add = (x, y) => x + y
    const multiply = (x, y) => x * y
    
    const c$  = M.periodic(1000, 1).scan(add, 0)
    const m$  = M.periodic(2000, 2).scan(multiply, 2)
    // c$.observe(x => console.log(x))
    // m$.observe(x => console.log(x))


    const counter = i => new Promise(resolve => setTimeout(() => resolve(i), 5000))
    // const c$ = M.unfold( i => counter(i).then(content => content), 1)
    // c$.observe( (value) => console.log(value))
    const cGenerator$ = function* (){
        for (let i = 0;; i++){
            if ( i %2 == 0)
                yield counter(c$)
            else
                yield counter(m$)    

        }
    }
    const cG$ = M.generate(cGenerator$)
    cG$.switchLatest().observe(x => console.log(x))

}

higherOrder()


function delaying() {
    M.periodic(1000, 2).delay(1).observe(x => console.log(x))
}

delaying()